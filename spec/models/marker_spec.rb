require 'rails_helper'

RSpec.describe Marker, :type => :model do
  it "is successful for good coordinates" do
    map = Map.create!(title: 'test')
    m1 = Marker.create(name: 'test', lat: 0.0, lon: 0.0, map_id: map.id)
    m2 = Marker.create(name: 'test', lat: 0.0, lon: 0.0, map_id: map.id)
    expect(m1.valid?).to be_truthy
    expect(m2.valid?).to be_truthy
  end

  it "fails for bad coordinates" do
    map = Map.create!(title: 'test')
    m1 = Marker.create(name: 'test', lat: -800.0, lon: 0.0, map_id: map.id)
    m2 = Marker.create(name: 'test', lat: 0.0, lon: -800.0, map_id: map.id)
    expect(m1.valid?).to be_falsy
    expect(m2.valid?).to be_falsy
  end

  it "fails if map_id is not set" do
    m = Marker.create(name: 'test', lat: 0.0, lon: 0.0)
    expect(m.valid?).to be_falsy
  end
end