require 'rails_helper'

RSpec.describe Map, :type => :model do
  it "is successful for good coordinates" do
    map = Map.create(title: 'test', lat1: 0.0, lon1: 0.0, lat2: 0.0, lon2: 0.0)
    expect(map.valid?).to be_truthy
  end

  it "fails for bad coordinates" do
    map1 = Map.create(title: 'test', lat1: -800.0, lon1: 0.0, lat2: 0.0, lon2: 0.0)
    map2 = Map.create(title: 'test', lat1: 0.0, lon1: -800.0, lat2: 0.0, lon2: 0.0)
    map3 = Map.create(title: 'test', lat1: 0.0, lon1: 0.0, lat2: -800.0, lon2: 0.0)
    map4 = Map.create(title: 'test', lat1: 0.0, lon1: 0.0, lat2: 0.0, lon2: -800.0)
    expect(map1.valid?).to be_falsy
    expect(map2.valid?).to be_falsy
    expect(map3.valid?).to be_falsy
    expect(map4.valid?).to be_falsy
  end
end