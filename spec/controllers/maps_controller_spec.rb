require 'rails_helper'

RSpec.describe MapsController, :type => :controller do

  describe "GET #index" do
    context "when not logged in" do
      it "should redirect to sign in" do
        get :index
        expect(response).to redirect_to(new_user_session_path)
      end
    end

    context "when logged in" do
      login_user

      it "responds successfully with 200 status" do
        get :index
        expect(response).to be_success
        expect(response).to have_http_status(200)
      end

      it "renders the index template" do
        get :index
        expect(response).to render_template("index")
      end
    end
  end

  describe "POST #create" do
    context "when not logged in" do
      it "should redirect to sign in" do
        get :index
        expect(response).to redirect_to(new_user_session_path)
      end
    end

    context "when logged in" do
      login_user

      it "should create a new map given valid data" do
        post :create, map: {title: 'test', lon1: 0.0, lon2: 0.0, lat1: 0.0, lat2: 0.0}
        expect(response).to redirect_to(maps_path)
      end

      context "for invalid longitude" do

        it "renders the new template" do
          post :create, params: {map: {title: 'test', lon1: -800.0, lon2: 0.0, lat1: 0.0, lat2: 0.0}}
          expect(response).to render_template("new")
        end

        xit "outputs an error message" do
          post :create, params: {map: {title: 'test', lon1: -800.0, lon2: 0.0, lat1: 0.0, lat2: 0.0}}
          expect(response.body).to match /must be greater than or equal to -180/
        end

      end

      context "for invalid latitude" do

        it "renders the new template" do
          post :create, params: { map: {title: 'test', lon1: 0.0, lon2: -800.0, lat1: 0.0, lat2: 0.0} }
          expect(response).to render_template("new")
        end

        xit "outputs an error message" do
          post :create, params: { map: {title: 'test', lon1: 0.0, lon2: -800.0, lat1: 0.0, lat2: 0.0} }
          expect(response.body).to match /must be greater than or equal to -90/
        end

      end

    end
  end
end