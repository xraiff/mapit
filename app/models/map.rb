class Map < ApplicationRecord
  has_many :markers

  validates :lat1, numericality: { greater_than_or_equal_to:  -90, less_than_or_equal_to:  90 }
  validates :lon1, numericality: { greater_than_or_equal_to: -180, less_than_or_equal_to: 180 }
  validates :lat2, numericality: { greater_than_or_equal_to:  -90, less_than_or_equal_to:  90 }
  validates :lon2, numericality: { greater_than_or_equal_to: -180, less_than_or_equal_to: 180 }

end