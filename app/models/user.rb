class User < ApplicationRecord
  devise :database_authenticatable, :registerable, :rememberable, :validatable, :authentication_keys => [:email]

  has_many :maps
end
