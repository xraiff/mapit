class MapsController <  ApplicationController
  def index
    @maps = current_user.maps
  end

  def new
    @map = Map.new
  end

  def create
    @map = Map.new(map_params)

    if @map.save
      redirect_to :maps
    else
      flash.now[:error] = @map.errors.full_messages
      render action: 'new'
    end
  end

  def destroy
    map.destroy
    redirect_to :maps
  end

  def edit
    map
  end

  def marker
    @marker = Marker.create(marker_params.merge(:map_id => params[:map_id]))
    render json: @marker
  end

  def markers
    render json: map_markers
  end

  def show
    map
    @markers = map.markers
    @marker = Marker.new(:map_id => map.id)
  end

  def update
    map.update_attributes(map_params)
    redirect_to :maps
  end

  private

  def map
    @map ||= Map.find(params[:id])
  end

  def map_markers
    Map.find(params[:map_id]).markers
  end

  def map_params
    params.require(:map).permit(:title, :lon1, :lat1, :lon2, :lat2).merge(:user_id => current_user.id)
  end

  def marker_params
    params.require(:marker).permit(:name, :description, :residents, :lat, :lon)
  end
end
