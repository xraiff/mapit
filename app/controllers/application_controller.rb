class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  before_action :authenticate_user!

  def index
    redirect_to maps_path if user_signed_in?
    # @user_data = UserSerializer.new(current_user) if current_user
  end

end
