# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161005124448) do

  create_table "maps", force: :cascade do |t|
    t.string   "title",      limit: 255
    t.float    "lat1",                   default: 0.0, null: false
    t.float    "lon1",                   default: 0.0, null: false
    t.float    "lat2",                   default: 0.0, null: false
    t.float    "lon2",                   default: 0.0, null: false
    t.integer  "user_id",    limit: 4
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
  end

  create_table "markers", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.string   "description", limit: 1024
    t.integer  "residents",   limit: 4
    t.float    "lat",                      default: 0.0, null: false
    t.float    "lon",                      default: 0.0, null: false
    t.integer  "map_id",      limit: 4
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",               limit: 255, null: false
    t.string   "encrypted_password",  limit: 255, null: false
    t.datetime "remember_created_at"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
  end

end
