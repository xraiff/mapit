class CreateUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :users do |t|
      t.string   "email",                  limit: 255, null: false
      t.string   "encrypted_password",     limit: 255, null: false
      t.datetime "remember_created_at"
      t.timestamps
    end
  end
end
