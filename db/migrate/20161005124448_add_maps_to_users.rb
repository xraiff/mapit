class AddMapsToUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :maps do |t|
      t.string   "title",      limit: 255
      t.float   "lat1",        default: 0.0, null: false
      t.float   "lon1",        default: 0.0, null: false
      t.float   "lat2",        default: 0.0, null: false
      t.float   "lon2",        default: 0.0, null: false
      t.integer "user_id",     limit: 4
      t.timestamps
    end

    create_table :markers do |t|
      t.string   "name",          limit: 255
      t.string   "description",   limit: 1024
      t.integer "residents",      limit: 4
      t.float   "lat",            default: 0.0, null: false
      t.float   "lon",            default: 0.0, null: false
      t.integer "map_id",         limit: 4
      t.timestamps
    end

  end
end
