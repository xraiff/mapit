Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  devise_for :users

  resources :maps do
    get 'markers', :defaults => { :format => 'json' }
    post 'marker', :defaults => { :format => 'json' }
  end

  root :to => 'application#index'
end
